#!/usr/bin/env zx

// $.verbose = false;

const file = process.argv[3];
console.log(file);

const uuid = (await $`uuidgen`).stdout.trim();

const sqsMessage = {
  originalName: file,
  uuid,
};

console.log(sqsMessage);
await $`aws s3 cp ${file} s3://poc-convert/${uuid}`;
await $`aws s3 ls s3://poc-convert`;

const p = $`aws sqs send-message --queue-url https://sqs.us-east-1.amazonaws.com/587562840274/poc-convert-jobs --message-body file:///dev/stdin`;
p.stdin.write(JSON.stringify(sqsMessage));
p.stdin.end();
await p
