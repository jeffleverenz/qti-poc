# qti-poc

## Overview

user -> upload qti zip -> s3 -> send sqs -> worker recv sqs -> process file -> ??

## Setup

* zx installed locally, npm i -g zx
* aws credentials for provisioning and accessing resources
* basic provisioned s3 bucket
* basic provisioned sqs queue

## Build

Build the worker

    cd worker
    docker build -t otus/qti-poc-worker .

## Run

Run the image in a terminal, add `-d` to detach:

    docker run -it \
      -e AWS_ACCESS_KEY_ID=<key> \
      -e AWS_SECRET_ACCESS_KEY=<secret> \
      -e AWS_DEFAULT_REGION=us-east-1 \
      -e S3_BUCKET_URL=<s3_root> \
      -e SQS_URL=<sqs_url> \
      otus/qti-poc-worker

The worker is now listening for SQS messages.

## Use

To process a QTI zip, it needs to be sent to the SQS queue. This POC uses s3 to hold sqs message
payload data, since sqs message sizes are limited. Note Amazon provides an [SQS Extended Client
Library for Java] that would provide a more complete solution.

To send a message, use the send-message.mjs script from your terminal:

    send-message.mjs

### Local Processing

You can also use the container to locally process a file. Drop the file into a qti directory and
provide an output working directory, then:

    docker run -it -v $(pwd)/qti:/usr/src/app/qti -v $(pwd)/working:/usr/src/app/working otus/qti-poc-worker


[SQS Extended Client Library for Java]: https://docs.aws.amazon.com/AWSSimpleQueueService/latest/SQSDeveloperGuide/sqs-s3-messages.html
