#!/usr/bin/env zx

// $.verbose = false;

const s3BucketUrl = process.env['S3_BUCKET_URL'];
const sqsUrl = process.env['SQS_URL'];

const hasQtiDir = await $`[[ -d qti ]]`.then(() => true).catch(() => false);

async function processFile(file) {
    await $`rm -rf working/* && mkdir -p working/data working/data/input working/data/output`;
    await $`cat ${file} | bsdtar -C working/data/input -xvf -`;
    await $`cd working && ../learnosity-qti/bin/mo convert:to:learnosity --organisation_id 335 --item-reference-source filename`;

    const out = {
      success: true,
      completedTimestamp: (new Date()).toISOString(),
      result: (await $`cd working && find . -type f | sort`).stdout,
      errors: undefined,
    };

    console.log('done processing, example output payload:');
    console.log(out);
}

async function receiveMessage() {
  return await $`aws sqs receive-message --queue-url ${sqsUrl} --wait-time-seconds 5 --max-number-of-messages 1`;
}

async function deleteMessage(receiveHandle) {
  return await $`aws sqs delete-message --queue-url ${sqsUrl} --receipt-handle ${receiveHandle}`;
}


if (hasQtiDir) {
  // just get the first found for now
  const zips = (await $`find qti -type f -maxdepth 1 -name "*.zip" |sort|head -n 1`).stdout.trim().split('\n');
  zips.forEach(async (file) => {
    console.log(`Processing ${file}`);
    await processFile(file);
  });
} else {
  console.log('listening to queue');

  while (true) {
    console.log(`....Checking for messages...`);
    const m = await receiveMessage();

    if (m.stdout !== '') {
      const message = JSON.parse(m.stdout).Messages[0];
      console.log(`Received MessageId ${message.MessageId}`);
      console.log(message);

      console.log('message body:');
      const msgJson = JSON.parse(message.Body);
      console.log(msgJson);

      console.log('unpacking zip');
      await $`rm -rf working && mkdir -p working`;

      await $`rm -rf working && mkdir -p working/data working/data/input working/data/output`;
      await $`aws s3 cp ${s3BucketUrl}/${msgJson.uuid} - | bsdtar -C working/data/input -xvf -`;
      await $`cd working && ../learnosity-qti/bin/mo convert:to:learnosity --organisation_id 335 --item-reference-source filename`;

      const out = {
        ...msgJson,
        success: true,
        completedTimestamp: (new Date()).toISOString(),
        result: (await $`cd working && find . -type f | sort`).stdout,
        errors: undefined,
      };

      console.log('done processing, example output payload:');
      console.log(out);

      await deleteMessage(message.ReceiptHandle);
    }
  }
}
